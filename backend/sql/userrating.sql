CREATE TABLE IF NOT EXISTS `userrating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_rating` tinyint(4) NOT NULL,
  `user_name` varchar(30)  NOT NULL,
  `user_comment` varchar(1000),
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
