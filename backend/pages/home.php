<?php
if (isset($_POST['formdata'])) {
    
    if (isset($_POST['range'])) {
        $range = $_POST['range'];
    }

    if (isset($_POST['name'])) {
        $user = $_POST['name'];
    }

    if (isset($_POST['comment'])) {
        $comment = $_POST['comment'];
    }

    $sql = file_get_contents($GLOBALS["sqluserrating"]);
    DBConnection::query($sql);

    $sql = "INSERT INTO userrating (user_rating,user_name,user_comment)
                   VALUES ('$range','$user','$comment');";
    DBConnection::query($sql);
}
    $sql = "SELECT * FROM userrating ORDER BY rating_id DESC;";
    $result = DBConnection::query($sql);
?>
<div class="jumbotron">
    <h1>Welcome Visitor!</h1>
    <p>You are currently searching for something you desperately need?</p>
    <p>Let us tell you, that <b>we possess everything</b> you could ever be in need of.
        Your dreams will come true, and to be so you only have to sell your soul. :)
        That sounds just awesome isn't it?</p>
    <h1>Information:</h1>
    <div class="table-responsive">
        <table class="table">
            <tbody>
                <tr>
                    <th>Open:</th>
                    <th>6am-6pm Every day.</th>
                    <th>Phone:</th>
                    <th>06 20 501 7393</th>
                </tr>
                <tr>
                    <th>Email:</th>
                    <th>tokdaniel4@gmail.com</th>
                    <th>Address:</th>
                    <th>6050 Lajosmizse Deák Ferenc út 2/b</th>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php if (!$result){}

else{?>
    <div id="commentbox" class="jumbotron">
        <h2>Ratings:</h2>
        <?php
            while ($row = $result->fetch_assoc()) {
                echo "<h4><span class='label label-primary'>" . $row['user_name'] . " (rated us " . $row['user_rating'] . "/10):</span>  " . $row['user_comment'] . "</h4>";
            }
        ?>
    </div>
<?php }?>

<div class="jumbotron">
    <form class="form-horizontal" role="form" method="post">
        <div class="form-group">
            <label for="range" class="col-sm-2 control-label">Rate us!</label>
            <div class="col-sm-10">
                <strong><div style="text-align: center" id="valBox">5</div></strong>
                <input name="range" type="range" min="1" max="10" step="1" value="5" oninput="showVal(this.value)" onchange="showVal(this.value)">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input name="name" type="text" class="form-control" placeholder="Your Name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Comment</label>
            <div class="col-sm-10">
                <textarea name="comment" class="form-control" rows="3" placeholder="Tell us what you think!"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </div>
        <input type="hidden" name="formdata" value="set">
    </form>
</div>

