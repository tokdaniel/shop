<?php

require_once 'globals.php';

class Template {

    private $file;

    function __construct($name) {
        $this->file = ROOT . "/backend/pages/$name.php";
    }

    function renderTemplate() {
        require $this->file;
    }

}

class DBConnection {

    private static $con;
    private static $noDBcon;

    private function __construct() {
        throw new SingletonException("Constructor cannot be called!");
    }

    static function init() {

        $database = file_get_contents($GLOBALS["sqlmyshopdb"]);

        self::$noDBcon = new mysqli($GLOBALS['sqlServer'], $GLOBALS['sqlUser'], $GLOBALS['sqlPassword']);
        self::$noDBcon->query($database);

        self::$con = new mysqli($GLOBALS['sqlServer'], $GLOBALS['sqlUser'], $GLOBALS['sqlPassword'], $GLOBALS['sqlDatabase']);
        if (self::$con->connect_errno) {
            die("Failed to connect to MySQL: " . self::$con->connect_error);
            //TODO: redirect to error page
        }
    }

    public static function query($sql) {
        return self::getConnection()->query($sql);
    }

    public static function getConnection() {
        if (!isset(self::$con)) {
            self::init();
        }

        return self::$con;
    }

    public function __destruct() {
        if (isset(self::$con)) {
            self::$con->close();
        }
    }

}
